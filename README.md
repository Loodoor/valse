![Logo](valse.png)

# Valse

Valse est un framework MVC pour créer des sites web avec le langage Vala. Il fonctionne 
aussi bien sous Windows, Linux ou Mac OS X, avec Vala 0.20 minimum.

# Une démo ?

*main.vala* :

```csharp
using Valse;

void main () {
    Router r = new Router ();
    r.register ("home", new HomeController ());
    r.listen ();
    // vous pouvez accéder à votre serveur sur http://localhost/
}
```

*home-controller.vala* :

```csharp
using Valse

public class HomeController : Controller {
    public HomeController () {
        this.add_action ("index", this.index);
    }
    
    public Response index () {
        return page ("demo.wala");
    }
}
```

*demo.wala* :

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Démo de Valse</title>
    </head>
    <body>
        <h1>Valse, c'est trop bien !</h1>
    </body>
</html>
```