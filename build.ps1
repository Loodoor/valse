try {
    rm test/serv.exe -ErrorAction Stop
}
catch {
    echo "serv.exe n'existe pas, impossible de le supprimer."
}

valac --pkg libsoup-2.4 --pkg gee-1.0 --pkg sqlite3 valse/*.vala valse/core/*.vala valse/wala/*.vala --library=out/valse -H out/valse.h -X -fPIC -X -shared -o out/valse.dll -X -w


If ($lastexitcode -eq 0) {
    # la compilation de valse a réussi
    echo " "
    echo "--- Valse build succeded ---"
    echo " "

    cp ./out/valse.dll ./test
    cp ./out/valse.h ./test
    cp ./out/valse.vapi ./test

    cd ./test
    valac *.vala --vapidir=. --pkg valse --pkg gee-1.0 --pkg libsoup-2.4 -X "-I." -X valse.dll -o serv.exe --save-temps

    If ($lastexitcode -eq 0) {
        echo " "
        echo "--- Server is starting ---"
        echo " "
        start "http://localhost:8088"
        ./serv
    } Else {
        echo " "
        echo "--- Server build failed ---"
        echo " "
    }
    cd ../

} Else {
    echo " "
    echo "--- Valse build failed ---"
    echo " "
}
