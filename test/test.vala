using Valse;

void main (string[] args) {

    Router r = new Router (8088);

    ZdSController zds = new ZdSController ();
    r.register ("zds", zds);
    r.register ("home", zds);

    r.listen ();

}
