using Valse;

public class ZdSModel : Object {

    public Many<string> contributors {get; set; default = new Many<string>.from_array ({"Wizix", "Folaefolc", "Bat'"});}

    public Many<int> zap {get; set; default = new Many<int>.from_array ({77, 987, 7});}

    public Many<Test> truc {get; set; default = new Many<Test>.from_array ({new Test ()});}

    public string greeter {get; set; default = "yey";}

    public string message {get; set; default = "youp";}

    public string question {get; set; default = "hey";}

    public string low {get; set; default = "test";}

    public int ze_reponse {get; set; default = 42;}

    public double taille {get; set; default = 1.7;}

    public double randomz {get; set; default = 223.7;}

}

public class Test : Object {
    public string blob {get; set; default = "chouette";}
}
