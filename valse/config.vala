namespace Valse {
    const string NAME = "Valse";
    const string VERSION = "-dev";

    /**
    * Comment
    */
    const bool DEBUG = true;
    const string DEBUG_DATE_FORMAT = "%d/%b/%Y %T";
    const int EXIT_SUCCESS = 1;
    const int EXIT_ON_ERROR = -1;
}