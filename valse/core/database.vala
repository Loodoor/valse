using Sqlite;
using Gee;

namespace Valse {

    /**
    * Helper for Sqlite
    */
    public class DataBase : Object {

        /**
        * The Sqlite DB which we interact
        */
        private Database db;

        /**
        * Handle the errors of the database actions
        *
        * @param ec The error code
        * @param message A message to display
        * @return EXIT_SUCCESS if no error was detected, else EXIT_ON_ERROR
        */
        private int handle_sql_errors(int ec, string message = "An error occured") {
            if (ec != Sqlite.OK) {
                stderr.printf (message, ": %d: %s\n", db.errcode (), db.errmsg ());
                return EXIT_ON_ERROR;
            }
            return EXIT_SUCCESS;
        }

        /**
        * Execute a given SQL query and return the status
        *
        * @param query The SQL query to execute
        * @param callback
        * @return EXIT_SUCCESS if no error was detected, else EXIT_ON_ERROR
        */
        private int execute_query(string query, Sqlite.Callback? callback = null) {
            string errmsg = "";
            int ec = db.exec(query, null, out errmsg);

            return handle_sql_errors(ec, errmsg);
        }

        /**
        * The constructor of the wrapper
        *
        * @param url The address of the DB
        */
        public DataBase (string url = Router.options.db_url) {
            // On initialise la BDD Sqlite
            int ec = Sqlite.Database.open (url, out this.db);
            string msg = "Can't open database";
            this.handle_sql_errors(ec, msg);
        }

        /**
        * Registers a new model in the DB
        *
        * @param model The {@link GLib.Type} of the model to register
        * @return The name of the created table
        */
        public string register<G> () {
            Type model = typeof (G);
            // query est la commande a éxécuter
            string query = "CREATE TABLE "; // crée une table ...
            query += model.name () + " ( "; // appelée par le nom du modèle
            bool first = true; // regarde si on est sur la première propriété

            foreach (ParamSpec spec in ((ObjectClass) model.class_ref ()).list_properties ()) {
                // on parcourt les propriétés
                string type = "NONE";
                string name = spec.name.replace ("-", "_");
                // On définit le type de la propriété dans la BDD
                switch (spec.value_type.name ()) {
                    case "gchararray":
                        type = "TEXT";
                        break;
                    case "gint":
                        type = "INT";
                        break;
                    case "gdouble":
                        type = "REAL";
                        break;
                    case "gfloat":
                        type = "REAL";
                        break;
                }
                // On regarde si on est dans la première propriété
                if (first) {
                    // si oui, on ne mets pas de virgule avant
                    query += "%s %s NOT NULL".printf (name, type);
                    first = false;
                } else {
                    // Si non, on en met une
                    query += ", %s %s NOT NULL".printf (name, type);
                }

            }
            // on fini la requête
            query += " );";

            // On éxécute la commande
            execute_query(query);

            // On revoie le nom du modèle dans la base, car il peut différer de celui dans le code
            return model.name ();
        }

        /**
        * Add an element in the DB.
        *
        * @param obj The element to add.
        * @param table The name of the table where we add the object.
        * @return The ID of the registered object
        */
        public int add (Object obj, string? table = null) {
            // on touve le nom de la table, si elle n'en a pas
            if (table == null) {
                table = obj.get_type ().name ();
            }

            // la commande à éxécuter
            string query = "INSERT INTO ";
            string vals = " ("; // contient les valeurs à insérer
            query += table; // on ajoute le nom de la table
            query += " (";

            int i = 1; // sert à déterminer
            foreach (ParamSpec spec in obj.get_class ().list_properties ()) {
                // on ajoute des quotes au début de la valuer si c'est du texte
                if (spec.value_type == typeof (string)) {
                    vals += "\"";
                }

                // on récupère la valeur de la variable
                query += spec.name.replace ("-", "_");
                GLib.Value val = GLib.Value (typeof (string));
                string prop = spec.name.replace ("-", "_");
                obj.get_property (prop, ref val);
                vals += (string) val;

                // on ferme les quotes si c'est du texte
                if (spec.value_type == typeof (string)) {
                    vals += "\"";
                }

                // on met une virgule si besoin
                if (i < obj.get_class ().list_properties ().length) {
                    query += ", ";
                    vals += ", ";
                } else {
                    query += ") ";
                    vals += ")";
                }

                i++;
            }

            // on ajoute les valeurs et ont ferme la commande
            query += "VALUES";
            query += vals;
            query += ";";

            // on exécute
            execute_query(query);

            // on renvoie l'ID de l'objet inséré
            return (int) db.last_insert_rowid ();
        }

        /**
        * Gives you the element corresponding to an ID.
        *
        * @param id The ID of the element to retrive.
        * @param type The {@link GLib.Type} that corresponds of the table where we look for the element.
        * @return The object corresponding to the ID.
        */
        public Object get_id<G> (int id) {
            Type type = typeof (G);
            // on crée l'objet à retoruner
            Object obj = Object.new (type);
            // on crée la requête
            string query = "SELECT * FROM ";
            query += type.name ();
            query += " WHERE rowid = %d".printf (id);

            // on exécute la commande
            execute_query(query, (ncol, vals, names) => {
                // on définit les propriétés de l'objet renvoyé
                for (int i = 0; i < ncol; i++) {
                    // pn prends le bon type
                    Type val_type = typeof (string);

                    foreach (ParamSpec spec in ((ObjectClass) type.class_ref ()).list_properties ()) {
                        if (spec.name.replace ("-", "_") == names [i]) {
                            val_type = spec.value_type;
                        }
                    }
                    // on défini les propriétés
                    GLib.Value val = GLib.Value (val_type);

                    switch (val.type ().name ()) {
                        case "gchararray":
                            val.set_string (vals [i]);
                            break;
                        case "gint":
                            val.set_int (int.parse (vals [i]));
                            break;
                        case "gdouble":
                            val.set_double (double.parse (vals [i]));
                            break;
                    }

                    obj.set_property (names [i], val);
                }
                return 0;

            });

            // on renvoie l'objet rempli avec les propriétés récupérées en BDD
            return obj;
        }

        /**
        * Delete the element corresponding to an ID.
        *
        * @param id The ID of the element to delete.
        * @param type The {@link GLib.Type} corresponding to the tble where we look for the element.
        */
        public void delete_id<G> (int id) {
            Type type = typeof (G);
            string query = "DELETE FROM ";
            query += type.name ();
            query += " WHERE rowid = %d;".printf (id);

            // on exécute
            execute_query(query);
        }

        /**
        * Gives you all the element of a table
        *
        * @param type The {@link GLib.Type} corresponding to the table where we look for.
        * @return A {@link Gee.ArrayList} contening all the element in the table.
        */
        public ArrayList<G> all<G> () {
            Type type = typeof (G);
            ArrayList<Object> result = new ArrayList<Object> ();
            // la commande a éxécuter
            string query = "SELECT * FROM %s;".printf (type.name ());

            execute_query(query, (ncol, vals, names) => {
                string? first_prop = ""; // le nom de la première propriété de l'objet
                Object obj = Object.new (type);

                for (int i = 0; i < ncol; i++) {
                    // on parcourt les résultats
                    Type val_type = typeof (string); // le type de la variable à définir

                    foreach (ParamSpec spec in ((ObjectClass) type.class_ref ()).list_properties ()) {

                        if (first_prop == "") { // on défini la première propriété si ce n'est pas le cas
                            first_prop = spec.name.replace ("-", "_");
                        }

                        if (spec.name.replace ("-", "_") == names [i]) {
                            val_type = spec.value_type; // on prends le bon type
                        }
                    }
                    // si on arrive sur la première propriété = on est sur un nouvel objet
                    if (first_prop != null && names [i] == first_prop) {
                        // on ajoute l'ancien objet et on le remplace
                        result.add (obj);
                        obj = Object.new (type);
                    }

                    // on défini la propriété
                    GLib.Value val = GLib.Value (val_type);

                    switch (val.type ().name ()) {
                        case "gchararray":
                            val.set_string (vals [i]);
                            break;
                        case "gint":
                            val.set_int (int.parse (vals [i]));
                            break;
                        case "gdouble":
                            val.set_double (double.parse (vals [i]));
                            break;
                    }

                    obj.set_property (names [i], val);
                }
                return 0;
            });

            return result;
        }

        /**
        * Update the element corresponding to an ID
        *
        * @param id The ID of the object to update
        * @param up_version The updated version of the model
        */
        public void update (int id, Object up_version) {
            // la requête à effectuer
            string query = "UPDATE ";
            query += up_version.get_type ().name ();
            query += " SET ";

            int i = 1;
            foreach (ParamSpec spec in up_version.get_class ().list_properties ()) {
                // on passe chaque propriété de l'objet
                query += spec.name.replace ("-", "_");

                query += " = ";

                // on met des quotes si c'est du texte
                if (spec.value_type == typeof (string)) {
                    query += "\"";
                }

                // on ajouute la valeur de la propriété
                GLib.Value val = GLib.Value (typeof (string));
                string prop = spec.name.replace ("-", "_");
                up_version.get_property (prop, ref val);
                query += (string) val;

                // on ferme les quotes si besoin
                if (spec.value_type == typeof (string)) {
                    query += "\"";
                }

                // on met une virgule si il faut
                if (i < up_version.get_class ().list_properties ().length) {
                    query += ", ";
                } else {
                    query += " ";
                }

                i++;
            }

            // on exécute la commande et on affiche les erreurs
            query += "WHERE rowid = %d;".printf (id);
            execute_query(query);
        }

    }

}
