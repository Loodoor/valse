using Soup;
using Gee;

namespace Valse {

    /**
    * A request sent by the user.
    */
    public class Request : Object {

        /**
        * The {@link Soup.Message} which is the base of the request.
        */
        private Message msg {get; set;}

        /**
        * The parameters of the request.
        */
        public HashMap<string, string> query {get; set; default = new HashMap<string, string> ();}

        /**
        * The request's method (GET, POST, PATCH, DELETE, etc).
        */
        public string method {get; private set; default = "GET";}

        /**
        * The IP adress of the visitor.
        */
        public string ip {get; private set; default = "127.0.0.1";}

        /**
        * The bidy of the request.
        */
        public string body {get; set;}

        /**
        * The parameters sent in a POST request and files uploaded.
        */
        public HashMap<string, string> form_data {get; private set; default = new HashMap<string, string> ();}

        /**
        * The ID of the request (e.g: /controller/action/id)
        */
        public string query_id {get; set;}

        /**
        * Creates a new request
        */
        public Request (Message msg, HashTable<string, string>? query, ClientContext ctx, string? id = null) {
            // récupère le corps de la requête
            this.msg = msg;
            this.body = (string) msg.request_body.data;

            // récupère la query id si elle existe
            if (id != null) {
                this.query_id = id;
            }

            // récupère le contenu des formulaires
            string contenttype = "";
            string filename = "";
            Buffer buff = new Buffer.take ("".data);
            HashTable<string, string> form = Form.decode_multipart (this.msg, null, out filename, out contenttype, out buff);

            form.foreach ((k,v) => {
                this.form_data [k] = v;
            });

            if (Regex.match_simple ("^multipart/", msg.request_headers.get_one ("Content-Type"))) {
                // TODO : c'est quoi cette condition vide ? O.o
            }
            // on récupère les paramètres de la query
            if (query != null) {
                query.foreach ((k, v) => {
                    this.query [k] = v;
                });
            }

            this.method = msg.method;
            this.ip = ctx.get_host ();
        }

        /**
        * Gets a specific header
        *
        * @param name The name of the header to get
        * @return The content of the header, or ERROR if it fails.
        */
        public string get_header (string name) {
            string val = "ERROR";
            val = this.msg.request_headers.get_one (name); // récupère le header qui va bien
            return val;
        }

    }

}
