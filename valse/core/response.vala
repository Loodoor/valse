using Gee;

namespace Valse {

    /**
    * A response to sent to the client.
    */
    public class Response : Object {

        /**
        * The response's headers.
        */
        public HashMap<string, string> headers {get; set; default = new HashMap<string, string> ();}

        /**
        * The HTTP code of the response.
        */
        public int error_code {get; set; default = 200;}

        /**
        * The mime-type of the response.
        */
        public string mime_type {get; set; default = "text/html";}

        /**
        * The body of the response.
        */
        public string body {get; set;}

        /**
        * Creates a new Response from {@link Soup.Message}
        *
        * @param msg The {@link Soup.Message} used to initialize {@link Valse.Response}
        */
        public Response (Soup.Message msg) {
            // récupère les headers de Soup
             msg.response_headers.foreach ((name, val) => {
                 this.headers [name] = val;
             });
             // un code d'erreur et un corps par défaut
             this.error_code = 200;
             this.body = "";
        }

        /**
        * Creates a new response from HTML.
        *
        * @param html The HTML used as the body of the new request.
        */
        public Response.from_html (string html) {
            this.body = html;
        }

    }

}
