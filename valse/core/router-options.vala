namespace Valse {

    /**
    * The options of a {@link Valse.Routeur}
    */
    public class RouterOptions : Object {

        /**
        * Specifies if we are in debug mode or not.
        *
        * If we debug, some messages will be shown.
        */
        public bool debug {get; set; default = true;}

        /**
        * The render engine used to render views
        *
        * It will, for example, be used in {@link Valse.Controller.page}
        */
        public RenderEngine render_engine {get; set; default = new Wala ();}

        /**
        * The default address for the DB.
        */
        public string db_url {get; set; default = "database.db";}

    }

}
