using Gee;

namespace Valse {

    public class Many<G> : Object {

        private G[] _arr {get; set; default = new G[] {};}

        public Many () {

        }

        public Many.from_array (G[] from) {
            this._arr = from;

        }

        public Many.from_arraylist (ArrayList<G> from) {
            foreach (G item in from) {
                this._arr [this._arr.length] = item;
            }
        }

        public ArrayList<string> to_string_array () {

            ArrayList<string> result = new ArrayList<string> ();
            Type t = typeof (G);

            foreach (G item in this._arr) {
                if (t.is_a (typeof (Object))) {
                } else if (t == typeof (string)) {
                    result.add ((string) item);
                } else if (t == typeof (int)) {
                    result.add (((int) item).to_string ());
                }
            }
            return result;
        }

    }
}
